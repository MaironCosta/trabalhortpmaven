package rtp.repository;

import java.util.LinkedHashSet;

public interface GenericRepository<T> {
	
	public T save (T t);
	
	public void remover (T t);
	
	public T getByCodigo (long codigo);
	
	public LinkedHashSet<T> getAll ();

}
