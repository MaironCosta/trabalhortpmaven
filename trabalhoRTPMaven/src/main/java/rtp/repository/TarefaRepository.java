package rtp.repository;

import java.util.LinkedHashSet;

import rtp.model.entity.Tarefa;
import rtp.model.entity.TarefaStatus;

public class TarefaRepository implements GenericRepository<Tarefa> {

	private static Long codigo = 0L;
	private static LinkedHashSet<Tarefa> listaTarefas = new LinkedHashSet<Tarefa>();
	
	public TarefaRepository() {
		// TODO Auto-generated constructor stub
	}
	
	public Tarefa save(Tarefa tarefa) {
		// TODO Auto-generated method stub
		
		tarefa.setCodigo(++codigo);
		listaTarefas.add(tarefa);
		
		return tarefa;
	}
	
	public LinkedHashSet<Tarefa> getByStatus(TarefaStatus tarefaStatus) {
		
		LinkedHashSet<Tarefa> tarefasPorStatus = new LinkedHashSet<Tarefa>();

		for (Tarefa t : listaTarefas) {

			if (t.getTarefaStatus().equals(tarefaStatus)) {
				tarefasPorStatus.add( t );
			}

		}

		return tarefasPorStatus;
	}
	
	public void remover(Tarefa t) {
		// TODO Auto-generated method stub

		listaTarefas.remove(t);

	}
	
	public Tarefa getByCodigo(long codigoTarefa) {
		// TODO Auto-generated method stub
		
		for (Tarefa t : listaTarefas) {

			if (t.getCodigo().longValue() == codigoTarefa) {
				return t;
			}
		}

		return null;
	}
	
	public LinkedHashSet<Tarefa> getAll() {
		// TODO Auto-generated method stub
		return listaTarefas;
	}

}
