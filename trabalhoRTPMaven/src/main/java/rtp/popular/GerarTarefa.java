package rtp.popular;

import java.util.Calendar;

import rtp.exceptions.ValidacaoException;
import rtp.model.IManager;
import rtp.model.ResponsavelManager;
import rtp.model.TarefaManager;
import rtp.model.entity.Responsavel;
import rtp.model.entity.Tarefa;

public class GerarTarefa {

	private IManager<Responsavel> responsavelManager;
	private TarefaManager tarefaManager;
	
	public GerarTarefa() {
		// TODO Auto-generated constructor stub
		responsavelManager = new ResponsavelManager();
		tarefaManager = new TarefaManager();
	}
	public IManager<Responsavel> getResponsavelManager() {
		return responsavelManager;
	}
	
	public IManager<Tarefa> getTarefaManager() {
		return tarefaManager;
	}
	
	public void execute () throws ValidacaoException {

		Calendar dtPrazo = Calendar.getInstance();

		dtPrazo.add(Calendar.DAY_OF_MONTH, 2);
		Tarefa tarefa = new Tarefa();
		tarefa.setDtPrazo((Calendar)dtPrazo.clone());
		tarefa.setResponsavel(responsavelManager.getByCodigo(1L));
		tarefa.setTitulo("Teste 01");
		tarefaManager.cadastrar(tarefa);

		dtPrazo.add(Calendar.DAY_OF_MONTH, 2);
		tarefa = new Tarefa();
		tarefa.setDtPrazo((Calendar)dtPrazo.clone());
		tarefa.setResponsavel(responsavelManager.getByCodigo(2L));
		tarefa.setTitulo("Teste 02");
		tarefaManager.cadastrar(tarefa);

		dtPrazo.add(Calendar.DAY_OF_MONTH, 2);
		tarefa = new Tarefa();
		tarefa.setDtPrazo((Calendar)dtPrazo.clone());
		tarefa.setResponsavel(responsavelManager.getByCodigo(3L));
		tarefa.setTitulo("Teste 03");
		tarefaManager.cadastrar(tarefa);
		
		tarefaManager.fecharTarefa(tarefa.getCodigo());

	}

}
