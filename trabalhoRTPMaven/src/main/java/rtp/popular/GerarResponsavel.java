package rtp.popular;

import rtp.exceptions.ValidacaoException;
import rtp.model.IManager;
import rtp.model.ResponsavelManager;
import rtp.model.entity.Responsavel;

public class GerarResponsavel {

	private IManager<Responsavel> manager = new ResponsavelManager();
	
	public GerarResponsavel() {
		// TODO Auto-generated constructor stub
	}
	
	public IManager<Responsavel> getManager() {
		return manager;
	}

	public void execute () throws ValidacaoException {
		
		Responsavel responsavel = new Responsavel();
		responsavel.setNome("Mairon");
		manager.cadastrar(responsavel);
		
		responsavel = new Responsavel();
		responsavel.setNome("Cassius");
		manager.cadastrar(responsavel);
		
		responsavel = new Responsavel();
		responsavel.setNome("Denis");
		manager.cadastrar(responsavel);
		
		responsavel = new Responsavel();
		responsavel.setNome("Leonardo");
		manager.cadastrar(responsavel);		
		
	}

}
