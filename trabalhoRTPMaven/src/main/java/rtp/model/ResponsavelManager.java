package rtp.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import rtp.exceptions.ValidacaoException;
import rtp.model.entity.Responsavel;
import rtp.repository.ResponsavelRepository;
import rtp.utils.Validacoes;

public class ResponsavelManager implements IManager<Responsavel> {

	private ResponsavelRepository responsavelRepository;
	
	public ResponsavelManager() {
		// TODO Auto-generated constructor stub
		this.inicializar();
	}
	
	private void inicializar () {
		
		responsavelRepository = new ResponsavelRepository();
		
	}
	
	public Responsavel cadastrar (Responsavel responsavel) throws ValidacaoException {
		
		this.validarCadastrar(responsavel);
		
		responsavelRepository.save(responsavel);
		
		return responsavel;
	}
	
	private void validarCadastrar (Responsavel responsavel) throws ValidacaoException {
		
		List<String> erros = new ArrayList<String>();
		
		if (Validacoes.isEmpty(responsavel.getNome())) {
			erros.add("Informe o nome do ResponsÃ¡vel.");
		}

		if (!Validacoes.isEmpty(erros)) {
			
			throw new ValidacaoException(erros);
			
		}
		
	}
	
	public Responsavel getByCodigo (long codigoResponsavel) {
		
		return responsavelRepository.getByCodigo(codigoResponsavel);
		
	}
	
	public LinkedHashSet<Responsavel> getAll () {
		return responsavelRepository.getAll();
	}
	
	public Responsavel getByMatricula (String matricula) throws ValidacaoException {		

		if (Validacoes.isEmpty(matricula)) throw new ValidacaoException("Informe a matricula");
		
		return responsavelRepository.getByMatricula(matricula);
		
	}

}
