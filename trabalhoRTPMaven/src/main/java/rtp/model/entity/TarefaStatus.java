package rtp.model.entity;

public enum TarefaStatus {

	ABERTA("ABERTAS"),
	FECHADAS("FECHADAS");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	private TarefaStatus (String descricao) {
		this.descricao = descricao;
	}

}
