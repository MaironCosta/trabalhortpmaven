package rtp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import rtp.exceptions.ValidacaoException;

public class UtilsData {

	public UtilsData() {
		// TODO Auto-generated constructor stub
	}
	
	public static String dataToString (Calendar data) {
		
		return dataToString(data.getTime());
		
	}
	
	public static String dataToString (Date data) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		return sdf.format(data.getTime());
		
	}
	
	public static Calendar stringByCalendar(String data) throws ParseException, ValidacaoException {

		if (Validacoes.isEmpty(data)) {
			
			throw new ValidacaoException("Data informa nula.");
			
		}
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		calendar.setTime(sdf.parse(data));

		return calendar;

	}
	
	public static boolean isBefeoreToday (Calendar data) throws ParseException, ValidacaoException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar hoje = Calendar.getInstance();

		hoje.setTime(stringByCalendar(sdf.format(hoje.getTime())).getTime());
		
		data.setTime(stringByCalendar(sdf.format(data.getTime())).getTime());
		
		if (data.before(hoje)) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isAfterToday (Calendar data) throws ParseException, ValidacaoException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar hoje = Calendar.getInstance();

		hoje.setTime(stringByCalendar(sdf.format(hoje.getTime())).getTime());
		
		data.setTime(stringByCalendar(sdf.format(data.getTime())).getTime());
		
		if (data.after(hoje)) {
			return true;
		}
		
		return false;
	}

}
