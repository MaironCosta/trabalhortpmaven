package rtp.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ValidacaoException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	private List<String> erros = new ArrayList<String>(1);	

	public ValidacaoException(List<String> erros) {
		super();
		this.erros = erros;
	}
	
	public ValidacaoException(String erro) {
		super();
		this.erros.add(erro);
	}

	public List<String> getErros() {
		
		if (erros != null) {
			
			for (String erro : erros) {
				
				System.out.println(erro);
				
			}
			
		}
		
		return erros;
	}

	public void setErros(List<String> erros) {
		this.erros = erros;
	}
	
}
