package rtp.servlets;

import java.io.IOException;
import java.util.LinkedHashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rtp.exceptions.ValidacaoException;
import rtp.model.TarefaManager;
import rtp.model.entity.Tarefa;
import rtp.model.entity.TarefaStatus;

/**
 * Servlet implementation class ListarTarefasServlet
 */
public class ListarTarefas extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarTarefas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String s = request.getParameter("status");

		TarefaManager tarefaManager = new TarefaManager();
		
		LinkedHashSet<Tarefa> result = null;
		try {
			
			result = tarefaManager.recuperarTarefaPorStatus(this.buscarTarefaStatus(s));
			
		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.setAttribute("tarefas", result);

		request.getRequestDispatcher("result.jsp").forward(request, response);
		
	}
	
	private TarefaStatus buscarTarefaStatus (String tarefaStatus) {

		for (TarefaStatus ts : TarefaStatus.values()) {
			
			if (ts.getDescricao().equalsIgnoreCase(tarefaStatus)) {

				// t = ts;
				System.out.println(ts);

				return ts;
			}
			
		}

		return null;

	}

}