package rtp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rtp.popular.PopularBase;


public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.home(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.home(request, response);
	}
	
	private void home (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Vai popular 777");
		
		PopularBase popularBase = new PopularBase();
		popularBase.execute();
		
		request.getSession().setAttribute("nomePagina", "Trabalho de RTP");
		
		request.getRequestDispatcher("home.jsp").forward(request, response);
		
	}

}
