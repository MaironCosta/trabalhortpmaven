<%@page import="java.util.*" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
<head>
   <title> ${nomePagina} </title>
</head>

<body>
<h1 align="center">Lista de tarefas</h1>
<p/>

<table>
       <thead>
              <tr>
                  <th>Titulo</th>
                  <th>Responsavel</th>
              </tr>
       </thead>
       <tbody>
              <c:forEach items="${tarefas}" var="t">
                  <tr>
                      <th>${t.titulo}</th>
                      <th>${t.responsavel.nome}</th>
                  </tr>
              </c:forEach>
       </tbody>
</table>
</body>
</html>